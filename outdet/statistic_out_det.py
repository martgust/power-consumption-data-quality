#####################################################################################################
#                                                                                                   #
#   Authors:                                                                                        #
#       Gustavo Felipe MARTIN NASCIMENTO, Frederic WURTZ,                                           #
#       Benoit DELINCHANT, Patrick KUO-PENG, Nelson JHOE BATISTELA                                  #
#   Contact:                                                                                        #
#       gustavo-felipe.martin-nascimento@g2elab.grenoble-inp.fr ; gustavofmn@hotmail.com            #
#                                                                                                   #                                                             
#####################################################################################################
#This python script defines functions to calculate bounderies and number of outliers by four methods#
# - Three-Sigma Rule                                                                                #
# - Boxplot                                                                                         #
# - Skewed Boxplot                                                                                  #
# - Adjusted Boxplot                                                                                #
#####################################################################################################

#Detect outliers with 3 Sigma rule (3 Standard Deviation)
def trois_sigma(Y1):
    import numpy as np 
#Calculates the standard deviation
    std1 = np.std(Y1)
#Calculates the mean value
    mean1= np.mean(Y1)
#Calculating Upper and Lower Bounds
    UPB=mean1+3*std1
    LWB=mean1-3*std1
#Detecting samples that are higher than the Upper Bound
    outls = [x1 for x1 in Y1 if x1 > UPB]
#Detecting of samples that are lower than the Lower Bound
    outlsb = [x1 for x1 in Y1 if x1 < LWB]
#Creating a flag for outliers
    X2=[]
    for x2 in Y1:
        if x2>UPB or x2>UPB :
            x2=1
        else:
            x2=0
        X2.append(x2)
#Caculating the number of measures out of the bounds
    SIGM_out=len(outls)+len(outlsb)
    return UPB,LWB,len(outls),len(outlsb),SIGM_out,outls,outlsb,X2
    
 #Detecting outliers with Boxplot Method
def Box_plot(Y1):

#Calculating the third quartile
    q3=Y1.quantile(0.75) #used 85% but the literature says 75%
#Calculating the first quartile
    q1=Y1.quantile(0.25) #used 15% but the literature says 25%
#Calculating the interquartile range
    iqr = q3 - q1
#Calculating lower and upper bounds
    LWBB = q1 -(3 * iqr) 
    UPBB = q3 +(3 * iqr)
#Detecting samples that are higher than the Upper Bound
    outl = [x1 for x1 in Y1 if x1 > UPBB]
#Detecting of samples that are lower than the Lower Bound
    outlme = [x1 for x1 in Y1 if x1 < LWBB]
#Creating a flag for outliers
    X2=[]
    for x2 in Y1:
        if x2 > UPBB or x2 < LWBB :
            x2 = 1
        else:
            x2 = 0
        X2.append(x2)    
#Caculating the number of measures out of the bounds
    BPM_out=len(outl)+len(outlme)
    return UPBB,LWBB,len(outl),len(outlme),BPM_out,outl,outlme,X2

#Detecting outliers with  Skewed Boxplot Method
def S_Box_plot(Y1):

#Calculating the third quartile
    q3=Y1.quantile(0.75) 
#Calculating the second quartile
    q2=Y1.quantile(0.5) 
#Calculating the first quartile
    q1=Y1.quantile(0.25) #
#Calculating the interquartile range
    Siqrl = q2 - q1
    Sirqu = q3 - q2
#Calculating lower and upper bounds
    LWBSB = q1 -(3 * Siqrl) 
    UPBSB = q3 +(3 * Sirqu)
#Detecting samples that are higher than the Upper Bound
    outls = [x1 for x1 in Y1 if x1 > UPBSB]
#Detecting of samples that are lower than the Lower Bound
    outlmes = [x1 for x1 in Y1 if x1 < LWBSB]
#Creating a flag for outliers
    X2=[]
    for x2 in Y1:
        if x2 > UPBSB or x2 < LWBSB :
            x2 = 1
        else:
            x2 = 0
        X2.append(x2)
#Caculating the number of measures out of the bounds
    SBPM_out=len(outls)+len(outlmes)
    return UPBSB,LWBSB,len(outls),len(outlmes),SBPM_out,outls,outlmes,X2
    
#Detecting outliers with Adjusted Boxplot Method
def Adj_Box_Plot(Y1):
        
#Calculating the third quartile
    q3=Y1.quantile(0.75) #used 85% but the literature says 75%
#Calculating the first quartile
    q1=Y1.quantile(0.25) #used 15% but the literature says 25%
#Calculating the interquartile range
    iqr = q3 - q1
#Adjusted Boxplot
    import statsmodels
    from statsmodels.stats.stattools import medcouple
    import math
#Calculating the medcouple
    medc=medcouple(Y1.values)
    medc1=medc.item()
#Calculating Upper and Lower Bounds
    if medc1 >= 0:
        UPBA=q3+(3*math.exp(3*medc1)*iqr)
        LWBA=q1-(3*math.exp(-4*medc1)*iqr)
    else:
        UPBA=q3+(3*math.exp(4*medc1)*iqr)
        LWBA=q1-(3*math.exp(-3*medc1)*iqr)
#Detecting samples that are higher than the Upper Bound
    outla = [x1 for x1 in Y1 if x1 > UPBA]
#Detecting of samples that are lower than the Lower Bound
    outld = [x1 for x1 in Y1 if x1 < LWBA]
#Creating a flag for outliers
    X2=[]
    for x2 in Y1:
        if x2 > UPBA or x2 < LWBA :
            x2 = 1
        else:
            x2 = 0
        X2.append(x2)
#Caculating the number of measures out of the bounds
    ABPM=len(outla)+len(outld)
    return UPBA,LWBA,len(outla),len(outld),ABPM,outla,outld,X2
    
 

def MAD(Y1):
    from statistics import median
    a=3
    b=1.4826
    r=median(Y1)
    f=[]
    for i in range(len(Y1)):
        f1=abs(Y1[i]-r)
        f.append(f1)
    MAD=b*median(f)
    UPMAD=r+a*MAD
    LWMAD=r-a*MAD
    #Detecting samples that are higher than the Upper Bound
    outlmadu = [x1 for x1 in Y1 if x1 > UPMAD]
#Detecting of samples that are lower than the Lower Bound
    outlmadl = [x1 for x1 in Y1 if x1 < LWMAD]
#Creating a flag for outliers
    X2=[]
    for x2 in Y1:
        if x2 > UPMAD or x2 < LWMAD :
            x2 = 1
        else:
            x2 = 0
        X2.append(x2)
#Caculating the number of measures out of the bounds
    MADPM=len(outlmadu)+len(outlmadl)
    #return UPBA,LWBA,len(outla),len(outld),ABPM,outla,outld,X2
    return UPMAD,LWMAD,len(outlmadu),len(outlmadl),MADPM,outlmadu,outlmadl,X2


 
    #Sumary of the results for the one timed search

#Search for outliers by each method one time
#Enter series to search outliers (can be forecast error) and ground_truth outliers
def detection(Y,GT_out):
    import pandas as pd
    tabela=pd.DataFrame([table('3 Sigma',Y,GT_out),table('MAD',Y,GT_out),table('BoxPlot',Y,GT_out),
                     table('Skewed BoxPlot',Y,GT_out),table('Adjusted BoxPlot',Y,GT_out)], 
    index=['3 Sigma','MAD','BoxPlot','Skewed BoxPlot','Adjusted BoxPlot'],
    columns=['Potential Outliers Detected','True Positives',
             'False Negatives','False Positives'])#,'Precision','Recall','F_Score'])
    #tabela['Total Misclassifications']=tabela['False Negatives']+tabela['False Positives']
    tabela['Precision']=tabela['True Positives']/(tabela['True Positives']+tabela['False Positives'])
    tabela['Recall']=tabela['True Positives']/(tabela['True Positives']+tabela['False Negatives'])
    tabela['F_Score']=2*(tabela['Precision']*tabela['Recall'])/(tabela['Precision']+tabela['Recall'])
    return tabela


def limites(Y1):
    import pandas as pd
    UPBT,LWBT,NOUT,NOLT,NOT,OUTUT,OUTLT,FLAGT=trois_sigma(Y1)
    UPBMAD,LWBMAD,NOUMAD,NOLMAD,NOMAD,OUTUMAD,OUTLMAD,FLAGMAD=MAD(Y1)
    UPBB,LWBB,NOUB,NOLB,NOB,OUTUB,OUTLB,FLAGB=Box_plot(Y1)
    UPBS,LWBS,NOUS,NOLS,NOS,OUTUS,OUTLS,FLAGS=S_Box_plot(Y1)
    UPBA,LWBA,NOUA,NOLA,NOA,OUTUA,OUTLA,FLAGA=Adj_Box_Plot(Y1)
#Table to compare Outliers detected by each method
    d=([UPBT,LWBT,NOUT,NOLT,NOT],
       [UPBMAD,LWBMAD,NOUMAD,NOLMAD,NOMAD],
       [UPBB,LWBB,NOUB,NOLB,NOB],
       [UPBS,LWBS,NOUS,NOLS,NOS],
       [UPBA,LWBA,NOUA,NOLA,NOA])
    limits=pd.DataFrame(data=d,columns=['Upper Bound','Lower Bound','Upper Outliers','Lower Ouliers','Total Outliers'],
                     index=['3 Sigma','MAD','BoxPlot','Skewed BoxPLot','Adjusted BoxPlot'])
    oi=([OUTUT,OUTLT],
        [OUTUMAD,OUTLMAD],
        [OUTUB,OUTLB],
        [OUTUS,OUTLS],
        [OUTUA,OUTLA])
    #FL=([FLAGT,FLAGB,FLAGS,FLAGA])
    FL1={'3 Sigma':FLAGT,'MAD':FLAGMAD,'BoxPlot':FLAGB,'Skewed BoxPlot':FLAGS,'Adjusted BoxPlot':FLAGA}
    FL=pd.DataFrame(data=FL1)
    #FL=FL.tranpose()
    return limits,oi,FL
 
#Compares outliers detected and the ground truth 
def table(U,Y1,GT_out):
    import numpy as np
    limits,oi,FL=limites(Y1)
    #FL['Ground Truth']=GT_out['Outliers'][4379:].values
    FL['Ground Truth']=GT_out['Outliers'][:len(FL)].values
    #Potential Outliers Detected
    TS_P=FL[U].sum()
    #Actual Outliers Detected
    #TS_A=np.where((FL[U] == 1) & (FL['Ground Truth'] == 1), 1, 0).sum()
    TS_A=len(FL.loc[(FL[U] == 1) & (FL['Ground Truth'] == 1)])
    #Undetected Outliers 
    #TS_U=np.where((FL[U] == 0) & (FL['Ground Truth'] == 1), 1, 0).sum()
    TS_U=len(FL.loc[(FL[U] == 0) & (FL['Ground Truth'] == 1)])
    #Normal Samples Misclassified as Outliers
    #TS_N=np.where((FL[U] == 1) & (FL['Ground Truth'] == 0), 1, 0).sum()
    TS_N=len(FL.loc[(FL[U] == 1) & (FL['Ground Truth'] == 0)])
    #Precision=(TS_A)/(TS_A+TS_N)
    #Recall=(TS_A)/(TS_A+TS_U)
    #F_Score=2*((Precision*Recall)/(Precision+Recall))
    return [TS_P,TS_A,TS_U,TS_N]#, Precision, Recall, F_Score] #FL 
    
