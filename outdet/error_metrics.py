#####################################################################################################
#                                                                                                   #
#   Authors:                                                                                        #
#       Gustavo Felipe MARTIN NASCIMENTO, Frederic WURTZ,                                           #
#       Benoit DELINCHANT, Patrick KUO-PENG, Nelson JHOE BATISTELA                                  #
#   Contact:                                                                                        #
#       gustavo-felipe.martin-nascimento@g2elab.grenoble-inp.fr ; gustavofmn@hotmail.com            #
#                                                                                                   #                                                             
#####################################################################################################
#This python script defines a function to calculate the following error metrics to evaluate the     #
#performance of the forecast methods                                                                #
# - Mean Absolute Error                                                                             #
# - Mean Squared Error                                                                              #
# - Root Mean Square Error                                                                          #
# - Mean Absolute Percentage Error                                                                  #
# - Mean Absolute Scaled Error                                                                      #
#####################################################################################################



#The function asks for the parameters: Actual, Predict and Method
#actual is the actual consumption data series
#predict is the result of a forecast
#actual and predict must be the same lenght
#method is a string

def metrics_error(actual,predict,method):
    import pandas as pd
    import numpy as np
    df3=pd.DataFrame({'Actual': actual, 'Predict': predict})
    df3['Naive']=df3["Actual"].shift(168)
    df3['Error']=abs(df3.Actual-df3.Predict)

#MAPE Mean Absolute Percentage Error
    df3['Percent_Error']=df3['Error']/df3['Actual']
#If Actual value is 0 but the forecast value is not, consider Percentage Error equals 100%
    df3['Percent_Error']=df3['Percent_Error'].replace(np.inf,1)
    MAPE=df3['Percent_Error'].mean()

#Mean Absolute Error
    from sklearn.metrics import mean_absolute_error
    mae=mean_absolute_error(actual, predict)

#Root-mean-square deviation
    from sklearn.metrics import mean_squared_error
    mse=mean_squared_error(actual, predict)

#Root-mean-square deviation
    rmse=mean_squared_error(actual, predict, squared=False)

#Mean Absoulute Scaled Error
    MAEnaive=abs(df3['Actual'][168:]-df3['Naive'][168:]).mean()
    MASE=mae/MAEnaive

    df4=pd.DataFrame({'MAE':mae,'MSE':mse,'RMSE':rmse,'MASE':MASE,'MAPE':MAPE},
                 index=[method])
    return df4.T, df3