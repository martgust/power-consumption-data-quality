"""
Source: https://riptutorial.com/pandas/example/25798/create-a-custom-calendar
"""
from pandas.tseries.offsets import Day
from pandas.tseries.holiday import AbstractHolidayCalendar, Holiday, EasterMonday, Easter


class Vacances(AbstractHolidayCalendar):
    """ Custom Holiday calendar for France based on https://en.wikipedia.org/wiki/Public_holidays_in_France
    """
    # defining the days which should be declared as holidays
    rules = [Holiday('New Years Day', month=1, day=1), EasterMonday, Holiday('Labour Day', month=5, day=1),
             Holiday('Victory in Europe Day', month=5, day=8),
             Holiday('Ascension Day', month=1, day=1, offset=[Easter(), Day(39)]),
             Holiday('Bastille Day', month=7, day=14), Holiday('Assumption of Mary to Heaven', month=8, day=15),
             Holiday('All Saints Day', month=11, day=1), Holiday('Armistice Day', month=11, day=11),
             Holiday('Christmas Day', month=12, day=25),
             Holiday('Vacances de Noel',month=12,day=23), Holiday('Vacances de Noel',month=12,day=23), Holiday('Vacances de Noel',month=12,day=24),
             Holiday('Vacances de Noel',month=12,day=26), Holiday('Vacances de Noel',month=12,day=27), Holiday('Vacances de Noel',month=12,day=28), 
             Holiday('Vacances de Noel',month=12,day=28), Holiday('Vacances de Noel',month=12,day=29), Holiday('Vacances de Noel',month=12,day=30), Holiday('Vacances de Noel',month=12,day=31),
             Holiday('Vacances de été',month=7,day=31), Holiday('Vacances de été',month=8,day=1), Holiday('Vacances de été',month=8,day=1),
             Holiday('Vacances de été',month=8,day=2), Holiday('Vacances de été',month=8,day=3), Holiday('Vacances de été',month=8,day=4),
             Holiday('Vacances de été',month=8,day=5), Holiday('Vacances de été',month=8,day=6), Holiday('Vacances de été',month=8,day=7),
             Holiday('Vacances de été',month=8,day=8), Holiday('Vacances de été',month=8,day=9), Holiday('Vacances de été',month=8,day=10),
             Holiday('Vacances de été',month=8,day=11), Holiday('Vacances de été',month=8,day=12), Holiday('Vacances de été',month=8,day=13),
             Holiday('Vacances de été',month=8,day=14), Holiday('Vacances de été',month=8,day=15)]
