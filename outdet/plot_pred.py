#Defines a function the plot predictions
def plot_predict(x_actual,y_actual,predict):
    import matplotlib.pyplot as plt
#Plot Actual and Predicted consumption
    plt.figure()
#plot actual consumption
    plt.plot(x_actual,y_actual,'blue')
#plot predicted consumption
    plt.plot(x_actual,predict,'red')
    plt.grid(color='black')
    plt.ylim(0,1000)
    plt.xlabel('Date')
    plt.ylabel('Energy consumption [kWh]')
    plt.legend(['Actual','Predictions'])
    plt.show()