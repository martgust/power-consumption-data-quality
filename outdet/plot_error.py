def plot_error_upper(method_bounds,data,method_error):
#Getting the Upper bounds calculated by each method
    import numpy as np
    import pandas as pd
    import matplotlib.pyplot as plt
#3_sigma
    t_sigma_ke = np.array([method_bounds.iloc[0][0] for i in range(len(data.index))])

#Boxplot
    b_plot_ke = np.array([method_bounds.iloc[1][0] for i in range(len(data.index))])

#Skewed Boxplot
    s_plot_ke = np.array([method_bounds.iloc[2][0] for i in range(len(data.index))])

#Adjusted Boxplot
    ab_plot_ke= np.array([method_bounds.iloc[3][0] for i in range(len(data.index))])

#Plotting the Lower Bounds
    plt.figure()


#Plot the Upper bounds
#3 sigma
    plt.plot(data.index, t_sigma_ke,'green',label='3 Sigma Limit')
#Boxplot
    plt.plot(data.index, b_plot_ke,'red',label='BoxPlot Limit')
#Skewed Boxplot
    plt.plot(data.index, s_plot_ke,'black',label='Skewed BoxPlot Limit')
#Adjusted Boxplot
    plt.plot(data.index, ab_plot_ke,'orange',label='Adjusted BoxPlot Limit')

#Plot actual consumption
    plt.plot(data.index,method_error,'blue',label='Actual')

    plt.grid(color='black')
    ymax=max(method_bounds['Upper Bound']+500)
    plt.ylim(0,ymax)
    plt.xlabel('Date')
    plt.ylabel('Absolute Error [kWh]')
    plt.legend()
    plt.show()
    return
    
    
def plot_error_lower(method_bounds,data,method_error):
#Getting the Upper bounds calculated by each method
    import numpy as np
    import pandas as pd
    import matplotlib.pyplot as plt
#3_sigma
    t_sigma_ke = np.array([method_bounds.iloc[0][1] for i in range(len(data.index))])

#Boxplot
    b_plot_ke = np.array([method_bounds.iloc[1][1] for i in range(len(data.index))])

#Skewed Boxplot
    s_plot_ke = np.array([method_bounds.iloc[2][1] for i in range(len(data.index))])

#Adjusted Boxplot
    ab_plot_ke= np.array([method_bounds.iloc[3][1] for i in range(len(data.index))])

#Plotting the Lower Bounds
    plt.figure()


#Plot the Upper bounds
#3 sigma
    plt.plot(data.index, t_sigma_ke,'green',label='3 Sigma Limit')
#Boxplot
    plt.plot(data.index, b_plot_ke,'red',label='BoxPlot Limit')
#Skewed Boxplot
    plt.plot(data.index, s_plot_ke,'black',label='Skewed BoxPlot Limit')
#Adjusted Boxplot
    plt.plot(data.index, ab_plot_ke,'orange',label='Adjusted BoxPlot Limit')

#Plot actual consumption
    plt.plot(data.index,method_error,'blue',label='Actual')

    plt.grid(color='black')
    ymin=min(method_bounds['Lower Bound']-100)
    plt.ylim(bottom=ymin)
    plt.xlabel('Date')
    plt.ylabel('Absolute Error [kWh]')
    plt.legend()
    plt.show()
    return