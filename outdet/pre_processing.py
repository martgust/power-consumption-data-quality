def open_data(file,ano):
    if file=='Actual':
        ano=str(ano)
        donne=load_data_actual(ano)
    elif file=='Adapted':
        donne=load_data_sint()
    elif file=='Adapted With Outliers':
        donne=load_data_sint_out()           
    return donne

def load_data_actual(ano1):
    import pandas as pd
    ano=str(ano1)
    tgbt1=pd.read_csv('./outdet/data/'+ano+'/771.csv',decimal=',',encoding='latin-1',sep=';')
    tgbt2=pd.read_csv('./outdet/data/'+ano+'/585.csv',decimal=',',encoding='latin-1',sep=';')
    temp=pd.read_csv('./outdet/data/'+ano+'/Temp.csv',decimal=',',encoding='latin-1',sep=';')
#Joins all data in one new dataframe called "data"
#Set new dataframe as TGBT1 data
    data=tgbt1
#Renames data columns
    data.columns=['Date','TGBT1']
#Renames TGBT2 columns
    tgbt2.columns=['Date','TGBT2']
#Inserts TGBT2 data in the dataframe "data"
    data['TGBT2']=tgbt2.TGBT2
#Transforms the date column in Datetime type
    data['Date']=pd.to_datetime(data.Date,dayfirst=True)
#Calculates the energy consumption every 10 minutes kW10min
    data['TGBT1']=data.TGBT1.diff()
    data['TGBT2']=data.TGBT2.diff()
    data=data.drop([0])
#Calculates building global energy consumption and stores in a column called "GreenEr"
    data['GreenEr']=data.sum(axis=1)
    data=data.fillna(method='bfill')
#Resamples the data from 10 minutes to 1 hour (hourly consumption kWh) and calls new dataframa "datahora"
    datahora = data.set_index('Date').resample('1H').sum()
#Takes only the 2017 samples
    datahora=datahora.head(8761)
#Reset the index after resampling
    datahora=datahora.reset_index()
#Joins Temperature date in the datahora dataframe
    datahora['Temperature']=temp.Temperature
    datahora=datahora.fillna(0)
    
    data=datahora
    data.index=datahora.set_index('Date').index.astype('datetime64[ns]')
    data=data[~data.index.duplicated(keep='first')]
#Drops unused columns
    data=data.drop(['TGBT1'],axis=1)
    data=data.drop(['TGBT2'],axis=1)
    data=data.drop(['Date'],axis=1)
    temp = data.drop(['GreenEr'],axis=1)
    
    #Renames columns "Actual is the actual electricity consumption"
    data = data.rename(columns={"GreenEr": "Actual"})

#In order do predict the consumption other variables are needed
#In this case the following variables will be used:
    #External Temperature

    #Daily average external temperature
#Calculates Mean Temperature per day
    temp['Temperature'] = temp['Temperature'].fillna(method = 'ffill')
    temp['T_avg']=temp['Temperature'].groupby(temp.index.date).mean()
#fill the average value in each day
    tempMean = temp['T_avg'].ffill()
#Joins average temperature in "data" dataframe
    data = data.join(tempMean, how='outer')

    #Electricity consumption one week earlier
    #data["lag(h-168)"] = data["Actual"].shift(168)

    #Hour of the day
    data['HourOfDay'] = data.index.hour   
    
    #Day of the week
    data['DayOfWeek'] = data.index.weekday    
    
    #Day of the year
    data['DayOfYear'] = data.index.dayofyear  

    #Month
    data['Month'] = data.index.month

    #If it is a holiday
    from outdet.frenchholidays import Vacances
    calendar = Vacances()
    holidays = calendar.holidays(start=data.index.min(), end=data.index.max()).date.tolist()
    data['TIMESTAMP'] = data.index.date
    data['Holiday'] = data['TIMESTAMP'].apply(lambda x: True if (x in holidays) else False)
    data = data.drop(['TIMESTAMP'], axis=1)
    data=data.dropna()
#Takes 2017 data
    data=data[-8761:]
    data["Holiday"] = data["Holiday"].astype(int)

#Load the Ground Thuth for outliers in the data   
    GT_out=pd.read_csv('./outdet/data/2017/GT_Out.csv',decimal=',',encoding='latin-1',sep=';')
    data=reverse_df(data)
    GT_out=reverse_df(GT_out).drop(['index'],axis=1)
    return data,GT_out

def reverse_df(d2):
    import pandas as pd
    d1=d2.iloc[::-1].reset_index()
    d1["Date"] = d1["Date"].values[::-1]
    d1['Date']=pd.to_datetime(d1.Date,dayfirst=True)
    d1 = d1.set_index('Date')
    return d1

    
def load_data_sint():
    import pandas as pd
    data=pd.read_csv('./outdet/data/dados_sint1.csv',encoding='latin-1',sep=',')
    data['Date']=pd.to_datetime(data.Date,dayfirst=True)
    data.index=data.set_index('Date').index.astype('datetime64[ns]')
    data=data.drop(['Date'],axis=1)
    data["Holiday"] = data["Holiday"].astype(int)
    
    
    return data


def load_data_sint_out():
    
    import pandas as pd
    data=pd.read_csv('./outdet/data/dados_sint_c_out1.csv',encoding='latin-1',sep=';')
    data['Date']=pd.to_datetime(data.Date,dayfirst=True)
    data.index=data.set_index('Date').index.astype('datetime64[ns]')
    data=data.drop(['Date'],axis=1)
    data["Holiday"] = data["Holiday"].astype(int)
    data=data.fillna(0)
    GT_out=data['Outliers']
    GT_out=pd.DataFrame(GT_out)
    GT_out=GT_out.reset_index()
    return data, GT_out